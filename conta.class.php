<?
class conta
{
    public $saldo = 0;
    public $titular;

    function __construct()
    {
        
    }
    function titular($titular){
        $this->titular = $titular;
    }

    function sacar($valor)
    {
        if ($this->saldo > 0 && $this->saldo >= $valor) {
            $_SESSION['CONTA_BANCARIA'][$this->titular]['saldoAtual'] = $this->saldo;
            $_SESSION['CONTA_BANCARIA'][$this->titular]['saque'] = $valor;
            
            $this->saldo -= $valor;

            $_SESSION['CONTA_BANCARIA'][$this->titular]['novoSaldo'] = $this->saldo;;
        } 
        else {
            echo "saldo insuficiente";
        }
    }
    function depositar($valor)
    {
        $_SESSION['CONTA_BANCARIA'][$this->titular]['deposito'] = $valor;
        $this->saldo += $valor;
        $_SESSION['CONTA_BANCARIA'][$this->titular]['novoSaldoComDeposito'] = $this->saldo;
    }
    function verSaldo()
    {
        echo "saldo atual: " . $this->saldo;
    }
}


?>